import React from 'react';
import BarraNavegacion from './components/BarraNavegacion'
import pagina1 from './pages/pagina1'
import pagina2 from './pages/pagina2'
import pagina3 from './pages/pagina3'




function App() {

  return (
    <div>
      <BarraNavegacion
       pagina1={pagina1}
       pagina2={pagina2}
       pagina3={pagina3}
       />
    </div>
  );
}

export default App;
