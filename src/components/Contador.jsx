import React, { useState } from 'react';




function Contador(props) {

const [numero, setNumero] = useState(0)


const suma = () => {
    setNumero(numero + 1)
}

const resta = () => {
    setNumero(numero - 1)
}


    return (
        <div>
            <p>{numero}</p>
            <button onClick={suma} >agregar</button>
            <button onClick={resta} >descontar</button>
        </div>
    );
}

export default Contador;
