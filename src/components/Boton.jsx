import React, {useState} from 'react';

const styles={
  letraRoja:{
    color:"red"
  },
  letraAzul:{
    color:"blue"
  }
}


function Boton1(props) {

  const {accion, texto, colorLetra} = props;


  return (
    <div>

    

     <button style={colorLetra === "rojo" ? styles.letraRoja : colorLetra === "azul" ? styles.letraAzul : null }  onClick={accion}>{texto}</button>
    
    </div>
  );
}

export default Boton1;
